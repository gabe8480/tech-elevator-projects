<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Chase Java
  Date: 8/11/2020
  Time: 3:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Review</title>
</head>
<body>
<h1>Enter A Review</h1>
<c:url var="formAction" value="/reviewInput" />
<form method="POST" action="${formAction}">
    <div>
        <label for="username">UserName</label>
        <input type="text" name="username" id="username" value="" required/>
        <label for="rating">Rating</label>
        <input type="text" name="rating" id="rating" value="" required/>
        <label for="title">Title</label>
        <input type="text" name="title" id="title" value="" required/>
        <label for="review">Review</label>
        <input type="textArea" name="text" id="review" value="" required/>
        <input class="formSubmitButton" type="submit" value="Submit Review"/>
    </div>
</form>

</body>
</html>
