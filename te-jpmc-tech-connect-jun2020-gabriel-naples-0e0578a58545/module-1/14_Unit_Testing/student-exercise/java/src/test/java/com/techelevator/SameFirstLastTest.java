package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SameFirstLastTest {

    private SameFirstLast same;

    @Before
    public void setUp() throws Exception {
        same = new SameFirstLast();
    }

    @Test
    public void isItTheSameNullArray() {
        int[] nums = new int[]{};
        assertFalse(same.isItTheSame(nums));
    }

    @Test
    public void isItTheSameOneIndexArray() {
        int[] nums = new int[]{1};
        assertTrue(same.isItTheSame(nums));
    }

    @Test
    public void isItTheSameLongArrayDifferent() {
        int[] nums = new int[]{1,2,3,0};
        assertFalse(same.isItTheSame(nums));
    }

    @Test
    public void isItTheSameLongArraySamet() {
        int[] nums = new int[]{1,2,3,0,1};
        assertTrue(same.isItTheSame(nums));
    }
}