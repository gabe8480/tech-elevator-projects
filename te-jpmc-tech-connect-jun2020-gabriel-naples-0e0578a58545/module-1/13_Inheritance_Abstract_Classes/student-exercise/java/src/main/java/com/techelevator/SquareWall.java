package com.techelevator;

public class SquareWall extends RectangleWall{

    private int sideLength;

    //Constructors
    public SquareWall(){}

    public SquareWall(String name, String color, int sideLength){
        super(name, color, sideLength, sideLength);
        this.sideLength = sideLength;
    }

    //Methods
    public String toString(){
        String sideLengthStr = Integer.toString(sideLength);
        String str = super.getName() + " (" + sideLengthStr + "x" + sideLengthStr + ") square";
        return str;
    }

}


