package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DateFashionTest {

    private DateFashion date;

    @Before
    public void setUp() throws Exception {
        date = new DateFashion();
    }

    @Test
    public void getATableOneVeryStylishOtherAboveTwo() {
        assertEquals(2, date.getATable(8, 3));
    }

    @Test
    public void getATableOneVeryStylishOtherBelowTwo() {
        assertEquals(0, date.getATable(8, 2));
    }

    @Test
    public void getATableBothAverageStyle() {
        assertEquals(1, date.getATable(5, 3));
    }
}