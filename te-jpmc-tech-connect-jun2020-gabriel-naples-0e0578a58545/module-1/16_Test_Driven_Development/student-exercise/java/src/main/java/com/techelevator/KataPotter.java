package com.techelevator;

import java.util.*;

public class KataPotter {

    public KataPotter() {
    }

    public double getCost(int[] books) {
        double cost = 0;
        double costDiscount = 1.0;
        int numOfBooks = 0;
        int previousBookValHolder = 0;
        boolean arrayEmpty = false;
        int[] bookBasket = new int[]{0,0,0,0,0};
        Stack<Integer> setList = new Stack<>();

        //builds the book basket off of book stream passed in
        for(int book : books){
            bookBasket[book-1] += 1;
        }
        //builds a stack of sets of books
        while (arrayEmpty == false) {
            arrayEmpty = true;
            numOfBooks = 0;
            for (int i = 0; i < bookBasket.length; i++) {
                if (bookBasket[i] > 0) {
                    bookBasket[i] -= 1;
                    numOfBooks += 1;
                    //continues the while loop if any books remain in the bookBasket
                    //when they're all 0 it will exit the while
                    if (bookBasket[i] >= 1) {
                        arrayEmpty = false;
                    }
                }
            }
            setList.push(numOfBooks);
        }
        //uses the stack to calculate the cost of each set
        while(!setList.empty()){
            numOfBooks = setList.pop();
            //peeks ahead to adjust cost of books for best price
            if(!setList.empty() && (setList.peek() == 5 && numOfBooks == 3)){
                numOfBooks += 1;
                previousBookValHolder = setList.pop() - 1;
                setList.push(previousBookValHolder);
            }
            if (numOfBooks == 2 || numOfBooks == 3) {
                costDiscount -= (numOfBooks - 1) * 0.05;
            } else if (numOfBooks == 4 || numOfBooks == 5) {
                costDiscount -= numOfBooks * 0.05;
            }
            cost += (numOfBooks * 8) * costDiscount;
            costDiscount = 1.0;
        }
        return cost;
    }
}

// half working code I was playing with before creating stack method. implemented some
 /*
            costDiscount = 1.0;
            if (numOfBooks == 2 || numOfBooks == 3) {
                costDiscount -= (numOfBooks - 1) * 0.05;
            } else if (numOfBooks == 4 || numOfBooks == 5) {
                costDiscount -= numOfBooks * 0.05;
            }*/
            /*
            if(arrayEmpty == true){
                if(previousBookValHolder - numOfBooks >= 2){
                    cost -= (previousBookValHolder * 8) * previousCostDiscount;
                    numOfBooks += 1;
                    previousBookValHolder -= 1;

                }
            }*/
            /*
            cost += (numOfBooks * 8) * costDiscount;
            previousBookValHolder = numOfBooks;
            previousCostDiscount = costDiscount;
            */