package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringBitsTest {

    private StringBits bits;

    @Before
    public void setUp() throws Exception {
        bits = new StringBits();
    }

    @Test
    public void getBitsNullString() {
        assertEquals("", bits.getBits(""));
    }

    @Test
    public void getBitsOneCharString() {
        assertEquals("H", bits.getBits("H"));
    }

    @Test
    public void getBitsBigString() {
        assertEquals("HHBROTHER", bits.getBits("HeHeBeReOeTeHeEeR"));
    }
}