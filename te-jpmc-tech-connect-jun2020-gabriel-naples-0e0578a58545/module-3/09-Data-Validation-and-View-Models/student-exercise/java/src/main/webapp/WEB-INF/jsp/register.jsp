<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="pageTitle" value="Home Page"/>
<%@include file="common/header.jspf" %>

<h2>Register For An Account</h2>

<c:url var="registerSubmitUrl" value="/register"/>
<form:form method="POST" action="${registerSubmitUrl}" modelAttribute="registration">

    <div>
        <label for="firstName">First Name</label>
        <form:input path="firstName"/>
        <form:errors path="firstName" cssClass="error"/>
    </div>
    <div>
        <label for="lastName">Last Name</label>
        <form:input path="lastName"/>
        <form:errors path="lastName" cssClass="error"/>
    </div>
    <div>
        <label for="email">Email</label>
        <form:input path="email"/>
        <form:errors path="email" cssClass="error"/>
    </div>
    <div>
        <label for="confirmEmail">Confirm Email</label>
        <form:input path="confirmEmail"/>
        <form:errors path="confirmEmail" cssClass="error"/>
        <form:errors path="emailMatching" cssClass="error"/>
    </div>
    <div>
        <label for="password">Password</label>
        <form:password path="password" placeholder="password"/>
        <form:errors path="password" cssClass="error"/>
    </div>
    <div>
        <label for="confirmPassword">Confirm Password</label>
        <form:password path="confirmPassword" placeholder="confirm password"/>
        <form:errors path="confirmPassword" cssClass="error"/>
        <form:errors path="passwordMatching" cssClass="error"/>
    </div>
    <div>
        <label for="birthDate">Birthday</label>
        <form:input path="birthDate"/>
        <form:errors path="birthDate" cssClass="error"/>
    </div>
    <div>
        <label for="numberOfTickets">Number Of Tickets</label>
        <form:input path="numberOfTickets" placeholder="0"/>
        <form:errors path="numberOfTickets" cssClass="error"/>
    </div>
    <div>
        <input type="submit" value="Sign Me Up!"/>
    </div>
</form:form>


<%@include file="common/footer.jspf" %>
