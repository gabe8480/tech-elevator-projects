package com.techelevator.entity;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TellerMachineTest {
    TellerMachine teller = new TellerMachine();

    @Test
    public void testValidateCardNumber_Valid_Starting_With_5_and_16_Long() {
        String cardNumber = "5123467891211413";
        assertTrue(teller.validateCardNumber(cardNumber));
    }

    @Test
    public void testValidateCardNumber_Invalid_Starting_With_5_and_15_Long() {
        String cardNumber = "512346789121141";
        assertFalse(teller.validateCardNumber(cardNumber));
    }

    @Test
    public void testValidateCardNumber_Valid_Starting_With_4_and_13_Long() {
        String cardNumber = "4123467891211";
        assertTrue(teller.validateCardNumber(cardNumber));
    }

    @Test
    public void testValidateCardNumber_Valid_Starting_With_4_and_16_Long() {
        String cardNumber = "4123467891211123";
        assertTrue(teller.validateCardNumber(cardNumber));
    }

    @Test
    public void testValidateCardNumber_Valid_Starting_With_3_and_4_Follows() {
        String cardNumber = "34123467891211123";
        assertTrue(teller.validateCardNumber(cardNumber));
    }

    @Test
    public void testValidateCardNumber_Valid_Starting_With_3_and_7_Follows() {
        String cardNumber = "37123467891211123";
        assertTrue(teller.validateCardNumber(cardNumber));
    }

    @Test
    public void getBalance_Deposit_100_Withdrawal_25() {
        teller = new TellerMachine("Name", BigDecimal.valueOf(100), BigDecimal.valueOf(25));
        assertEquals(BigDecimal.valueOf(75), teller.getBalance());
    }

}