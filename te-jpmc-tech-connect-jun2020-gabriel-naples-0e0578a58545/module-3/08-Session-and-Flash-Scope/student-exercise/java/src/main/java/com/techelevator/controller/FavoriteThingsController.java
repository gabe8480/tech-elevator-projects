package com.techelevator.controller;

import com.techelevator.model.FavoriteThings;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/FavoriteThings")
public class FavoriteThingsController {

	@RequestMapping(path = "/Page1", method = RequestMethod.GET)
	public String getPage1() {
		return "page1";
	}

	@RequestMapping(path = "/Page1", method = RequestMethod.POST)
	public String getPage1Data(@RequestParam String favoriteColor, HttpSession session) {
		FavoriteThings favorites = new FavoriteThings();
		favorites.setFavoriteColor(favoriteColor);

		session.setAttribute("favorites", favorites);

		return "redirect:Page2";
	}

	@RequestMapping(path = "/Page2", method = RequestMethod.GET)
	public String getPage2() {
		return "page2";
	}

	@RequestMapping(path = "/Page2", method = RequestMethod.POST)
	public String getPage2Data(@RequestParam String favoriteFood, HttpSession session) {
		FavoriteThings favorites = (FavoriteThings) session.getAttribute("favorites");
		favorites.setFavoriteFood(favoriteFood);
		return "redirect:Page3";
	}

	@RequestMapping(path = "/Page3", method = RequestMethod.GET)
	public String getPage3(ModelMap map) {
		return "page3";
	}

	@RequestMapping(path = "/Page3", method = RequestMethod.POST)
	public String getPage3Data(@RequestParam String favoriteSeason, HttpSession session) {
		FavoriteThings favorites = (FavoriteThings) session.getAttribute("favorites");
		favorites.setFavoriteSeason(favoriteSeason);
		return "redirect:SummaryPage";
	}

	@RequestMapping(path = "/SummaryPage", method = RequestMethod.GET)
	public String getSummaryPage() {
		return "summaryPage";
	}



}
