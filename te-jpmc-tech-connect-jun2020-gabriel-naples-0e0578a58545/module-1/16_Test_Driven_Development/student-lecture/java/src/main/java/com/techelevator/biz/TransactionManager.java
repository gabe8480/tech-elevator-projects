package com.techelevator.biz;

import com.techelevator.entity.Transaction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionManager {

    private Map<Integer, List<Transaction>> transactions;

    public TransactionManager(){
        this.transactions = new HashMap<>();
    }

    public TransactionManager(Map<Integer, List<Transaction>> transactions){
        this.transactions = transactions;
    }

    public Map<Integer, List<Transaction>> getTransactions() {
        return transactions;
    }
}
