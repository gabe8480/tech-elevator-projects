package com.techelevator;

public class TriangleWall extends Wall{

    private int base;
    private int height;

    //Constructors
    public TriangleWall() {}

    public TriangleWall(String name, String color, int base, int height){
        super(name, color);
        this.base = base;
        this.height = height;
    }

    //Setters and Getters
    public int getBase() {
        return base;
    }

    public int getHeight() {
        return height;
    }

    //methods
    public int getArea() {//Overriding the abstract method in Wall
        return ((base*height)/2);
    }

    public String toString(){
        String str = super.getName() + " (" + Integer.toString(base) + "x" + Integer.toString(height) + ") triangle";
        return str;
    }



}
