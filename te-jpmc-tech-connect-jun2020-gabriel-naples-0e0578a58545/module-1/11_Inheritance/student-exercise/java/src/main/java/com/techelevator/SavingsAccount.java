package com.techelevator;

import java.math.BigDecimal;

public class SavingsAccount extends BankAccount{

    //Constructors
    public SavingsAccount(String accountHolderName, String accountNumber){
        super(accountHolderName, accountNumber);
    }

    public SavingsAccount(String accountHolderName, String accountNumber, int balance){
        super(accountHolderName, accountNumber, balance);
    }

    //Methods
    @Override
    public int withdraw(int amountToWithdraw) {
        if(amountToWithdraw <= super.getBalance()) {
            if (super.getBalance() - amountToWithdraw < 150) {
                amountToWithdraw += 2;
            }
            return super.withdraw(amountToWithdraw);
        }
        return super.getBalance();
    }

}
