package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Less20Test {

    private Less20 less;

    @Before
    public void setUp() throws Exception {
        less = new Less20();
    }

    @Test
    public void isLessThanMultipleOf20TwoLess() {
        assertTrue(less.isLessThanMultipleOf20(18));
    }

    @Test
    public void isLessThanMultipleOf20OneLess() {
        assertTrue(less.isLessThanMultipleOf20(19));
    }

    @Test
    public void isLessThanMultipleOf20ThreeLess() {
        assertFalse(less.isLessThanMultipleOf20(17));
    }

    @Test
    public void isLessThanMultipleOf20Over() {
        assertFalse(less.isLessThanMultipleOf20(21));
    }
}