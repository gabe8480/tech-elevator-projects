package com.techelevator;

public class Driver {

    public static void main(String[] args){
        int[] grades = new int[]{10,11};
        GradeManager myManager = new GradeManager(grades);
        double averageScore = myManager.averageGrade();
        System.out.println(averageScore);
    }

}
