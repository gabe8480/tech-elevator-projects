package com.techelevator;

import java.util.ArrayList;
import java.util.List;

public class BankCustomer {

    private String name;
    private String address;
    private String phoneNumber;
    private Accountable[] accounts;
    //Bank customer 'Has-A' list of accounts (Accountables)
    private List<Accountable> accountList; //dynamic list to store accounts as they're added

    //constructors
    public BankCustomer(){
        accountList = new ArrayList<>(); //initialize List object when constructed
}

    //Setters and Getters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Accountable[] getAccounts() {
        //instantiates array the size of the accounts List
        accounts = new Accountable[accountList.size()];
        //iterates through each list item and adds it to the array
        for(int i = 0; i < accountList.size(); i++){
            accounts[i] = accountList.get(i);
        }
        return accounts; // returns array
    }

    //Methods
    public void addAccount (Accountable newAccount){
        accountList.add(newAccount);
    }

    public boolean isVip(){
        int balance = 0;
        for (Accountable account: accountList){
            balance += account.getBalance();
        }
        if(balance >= 25000){
            return true;
        }
        return false;
    }
}
