package com.techelevator;

import java.io.*;

public class FizzWriter {

	public static void main(String[] args) {
		int num = 1;
		//creates the file/opens the file
		File writeFile = new File("FizzBuzz.txt");
		//writes to the file
		try(PrintWriter filePrinter = new PrintWriter(writeFile);){
			while(num<=300) {
				filePrinter.println(fizzBuzz(num));
				num++;
			}
			System.out.println("FizzBuzz.txt has been created.");
		}catch(FileNotFoundException ex){
			ex.getMessage();
		}

	}

	public static String fizzBuzz(int num){
		if(num%3 == 0 && num%5 == 0){
			return "FizzBuzz";
		} else if(num % 3 == 0 || Integer.toString(num).contains("3")){
			return "Fizz";
		} else if (num % 5 == 0 || Integer.toString(num).contains("5")){
			return "Buzz";
		} else {
			return Integer.toString(num);
		}
	}

}
