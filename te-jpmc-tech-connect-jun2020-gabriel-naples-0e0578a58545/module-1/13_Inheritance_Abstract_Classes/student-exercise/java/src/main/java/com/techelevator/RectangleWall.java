package com.techelevator;

public class RectangleWall extends Wall{

    private int length;
    private int height;

    //constructor
    public RectangleWall(){}

    public RectangleWall(String name, String color, int length, int height){
        super(name, color);
        this.height = height;
        this.length = length;
    }
    //setters and getters

    public int getLength() {
        return length;
    }

    public int getHeight() {
        return height;
    }

    //Methods
    public int getArea(){//abstract method being implemented
        return (length * height);
    }

    public String toString(){
        String str = super.getName() + " (" + Integer.toString(length) + "x" + Integer.toString(height) + ") rectangle";
        return str;
    }

}
