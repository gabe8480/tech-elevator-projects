package com.techelevator.shoppingcart;

public class ShoppingCart {

    private int totalNumberOfItems;
    private double totalAmountOwed;

	public ShoppingCart(){
        this.totalAmountOwed = 0.00;
        this.totalNumberOfItems = 0;
    }

    public int getTotalNumberOfItems() {
        return totalNumberOfItems;
    }

    public double getTotalAmountOwed() {
        return totalAmountOwed;
    }

    public double getAveragePricePerItem(){
	    if (totalNumberOfItems == 0.0 && totalAmountOwed == 0.00){
	        return 0.00;
        }
	    return totalAmountOwed/totalNumberOfItems;
    }

    public void addItems(int numberOfItems, double pricePerItem){
	    totalNumberOfItems += numberOfItems;
	    totalAmountOwed += numberOfItems * pricePerItem;
    }

    public void empty(){
	    totalAmountOwed = 0;
	    totalNumberOfItems = 0;
    }


}
