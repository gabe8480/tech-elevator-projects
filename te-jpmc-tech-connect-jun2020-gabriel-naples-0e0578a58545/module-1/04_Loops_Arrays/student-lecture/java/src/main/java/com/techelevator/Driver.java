package com.techelevator;
import java.util.Scanner;

public class Driver {

    public static void main(String[] args){
        System.out.println("Enter your birthday: ");
        Scanner input = new Scanner(System.in);
        String birthday = input.nextLine();
        System.out.println("Your birthday is: " + birthday);
    }
}
