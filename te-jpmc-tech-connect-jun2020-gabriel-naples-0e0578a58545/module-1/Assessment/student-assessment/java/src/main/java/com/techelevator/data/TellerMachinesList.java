package com.techelevator.data;

import com.techelevator.entity.TellerMachine;

import java.util.List;

public class TellerMachinesList {
    private List<TellerMachine> tellerList;

    public TellerMachinesList(){}

    public TellerMachinesList(List<TellerMachine> tellerList){
        this.tellerList = tellerList;
    }

    public List<TellerMachine> getTellerList() {
        return tellerList;
    }

    public void setTellerList(List<TellerMachine> tellerList) {
        this.tellerList = tellerList;
    }
}
