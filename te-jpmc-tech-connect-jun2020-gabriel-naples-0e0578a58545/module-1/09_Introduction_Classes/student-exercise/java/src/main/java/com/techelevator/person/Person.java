package com.techelevator.person;

public class Person {

	private String firstName;
	private String lastName;
	private int age;

	public Person(){

    }

	//Setter and Getters
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    public void setAge(int age){
        this.age = age;
    }
    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public int getAge(){
        return age;
    }

    //Methods
    public String getFullName(){
        return firstName + " " + lastName;
    }

    public boolean isAdult(){
        if(age >= 18){
            return true;
        }
        return false;
    }
}
