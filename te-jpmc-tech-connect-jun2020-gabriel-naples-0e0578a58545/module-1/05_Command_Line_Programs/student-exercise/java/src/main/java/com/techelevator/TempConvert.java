package com.techelevator;
import java.util.Scanner;

public class TempConvert {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Please enter the temperature: ");
		int tempValue = Integer.parseInt(input.nextLine());
		System.out.println("Is the temperature in (C)elsius, or (F)ahrenheit?");
		char typeTemp = input.nextLine().charAt(0);
		tempConvert(tempValue, typeTemp);

	}

	public static void tempConvert(int temp, char type){
		float conversion = 0f;
		if (type == 'F' || type == 'f'){
			conversion = Math.round((temp-32)/(float)1.8);
			System.out.println(temp + "F" + " is " + (int)conversion + "C");
		} else if (type == 'C'|| type == 'c'){
			conversion = Math.round(temp * (float)1.8 + 32);
			System.out.println(temp + "C" + " is " + (int)conversion + "F");
		} else {
			System.out.println("Invalid Input.");
		}
	}

}
