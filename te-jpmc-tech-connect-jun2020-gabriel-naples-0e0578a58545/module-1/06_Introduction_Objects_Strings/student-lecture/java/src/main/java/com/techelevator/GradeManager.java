package com.techelevator;

public class GradeManager {

    int[] grades;//field of GradeManager Class

    //constructor - has same name as class, also note there is no return value
    //used to initialize the fields of our Class when we create an object
    public GradeManager(int[] gradesArray){
        grades = gradesArray;
    }

    //method of GradeManager Class
    public double averageGrade(){
        int sum = 0;
        for(int grade:grades){
            sum += grade;
        }
        return (double)sum/grades.length;
    }

}
