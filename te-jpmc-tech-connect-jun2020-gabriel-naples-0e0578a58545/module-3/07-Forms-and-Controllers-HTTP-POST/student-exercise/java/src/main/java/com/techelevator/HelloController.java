package com.techelevator;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.techelevator.model.Review;
import com.techelevator.model.ReviewDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {

	@Autowired
	ReviewDao reviewDao;

	@RequestMapping("/")
	public String displayHomePage(ModelMap modelHolder) {
		List<Review> reviews = reviewDao.getAllReviews();
		modelHolder.addAttribute(
				"localDateTimeFormat", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		modelHolder.put("reviews", reviews);
		return "homePage";
	}

}
