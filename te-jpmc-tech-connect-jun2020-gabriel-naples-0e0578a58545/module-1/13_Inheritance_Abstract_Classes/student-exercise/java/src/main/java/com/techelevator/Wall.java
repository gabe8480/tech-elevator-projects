package com.techelevator;

public abstract class Wall {

    private String name;
    private String color;

    //Constructors
    public Wall(){}

    public Wall(String name, String color){
        this.name = name;
        this.color = color;
    }

    //Setters and Getters
    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    //Methods
    public abstract int getArea();//Abstract method, no implementation here.



}
