package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FrontTimesTest {

    FrontTimes front;

    @Before
    public void setUp() throws Exception {
        front = new FrontTimes();
    }

    @Test
    public void generateStringZeroCopiesLessThanThree() {
        assertEquals("", front.generateString("we", 0));
    }

    @Test
    public void generateStringLessThanThree() {
        assertEquals("wewe", front.generateString("we", 2));
    }

    @Test
    public void generateStringLargerThanThree() {
        assertEquals("AbcAbcAbcAbc", front.generateString("Abcdef", 4));
    }
}