package com.techelevator.entity;

import java.math.BigDecimal;

public class Employee extends Person{

    private int empNo;
    private String role;
    private BigDecimal salary;

    public Employee() {
    }

    public Employee(String name, String email, int empNo, String role, BigDecimal salary) {
        super(name, email);
        this.empNo = empNo;
        this.role = role;
        this.salary = salary;
    }

    

}
