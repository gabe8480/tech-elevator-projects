package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Lucky13Test {

    private Lucky13 luck;

    @Before
    public void setUp() throws Exception {
        luck = new Lucky13();
    }

    @Test
    public void getLuckyContainsOne() {
        int[] nums = new int[]{1,2,4};
        assertFalse(luck.getLucky(nums));
    }

    @Test
    public void getLuckyContainsThree() {
        int[] nums = new int[]{3,2,4};
        assertFalse(luck.getLucky(nums));
    }

    @Test
    public void getLuckyContainsNeither() {
        int[] nums = new int[]{0,2,4};
        assertTrue(luck.getLucky(nums));
    }
}