<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="pageTitle" value="Home Page"/>
<%@include file="common/header.jspf" %>

<h2>Login</h2>

<c:url var="loginSubmitUrl" value="/login"/>
<form:form method="POST" action="${loginSubmitUrl}" modelAttribute="login">

    <div>
        <label for="email">Email Address</label>
        <form:input path="email" placeholder="email" />
        <form:errors path="email" cssClass="error"/>
    </div>
    <div>
        <label for="password">Password</label>
        <form:password path="password" placeholder="password"/>
        <form:errors path="password" cssClass="error"/>
    </div>
    <div>
        <input type="submit" value="Sign In!"/>
    </div>
</form:form>


<%@include file="common/footer.jspf" %>
