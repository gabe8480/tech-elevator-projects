package com.techelevator;

public class KataFizzBuzz {

    public KataFizzBuzz(){}

    public String fizzBuzz(int num){
        if((num%3 == 0 && num%5 == 0) || (Integer.toString(num).contains("3") && Integer.toString(num).contains("5"))){
            return "FizzBuzz";
        } else if(num % 3 == 0 || Integer.toString(num).contains("3")){
            return "Fizz";
        } else if (num % 5 == 0 || Integer.toString(num).contains("5")){
            return "Buzz";
        } else if(num >= 0 && num < 101){
            return Integer.toString(num);
        } else {
            return "";
        }
    }

}
