package com.techelevator;

public class Exercises {

	public static void main(String[] args) {

        /*
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch?
        */

		// ### EXAMPLE:
		int initialNumberOfBirds = 4;
		int birdsThatFlewAway = 1;
		int remainingNumberOfBirds = initialNumberOfBirds - birdsThatFlewAway;

        /*
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests?
        */

		// ### EXAMPLE:
		int numberOfBirds = 6;
		int numberOfNests = 3;
		int numberOfExtraBirds = numberOfBirds - numberOfNests;

        /*
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods?
        */
		int initialNumberOfRaccoons = 3;
		int raccoonsThatWentHome = 2;
		int raccoonsLeftInWoods = initialNumberOfRaccoons - raccoonsThatWentHome;

        /*
        4. There are 5 flowers and 3 bees. How many less bees than flowers?
        */
		int numberOfFlowers = 5;
		int numberOfBees = 3;
		int numberOfLessBeesThanFlowers = numberOfFlowers - numberOfBees;

        /*
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now?
        */
		int initialPigeonsEating = 1;
		int additionalPigeonsEating = 1;
		int totalPigeonsEating = initialPigeonsEating + additionalPigeonsEating;

        /*
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now?
        */
		int initialOwlsOnFence = 3;
		int additionalOwlsOnFence = 2;
		int totalOwlsOnFence = initialOwlsOnFence + additionalOwlsOnFence;
        /*
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home?
        */
		int initialBeaversWorking = 2;
		int beaversThatLeft = 1;
		int totalBeaversWorking = initialBeaversWorking - beaversThatLeft;
        /*
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all?
        */
		int initialToucansSitting = 2;
		int toucansThatJoined = 1;
		int totalToucansSitting = initialToucansSitting + toucansThatJoined;

        /*
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts?
        */
		int totalSquirrelsInTree = 4;
		int totalNutsInTree = 2;
		int numberOfSquirrelsOverNuts = totalSquirrelsInTree - totalNutsInTree;
        /*
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find?
        */
		int numberOfQuarters = 1;
		int numberOfDimes = 1;
		int numberOfNickels = 2;
		float valueOfQuarter = 0.25F;
		float valueOfDime = 0.10F;
		float valueOfNickel = 0.05F;
		float totalMoneyFound = ((numberOfQuarters * valueOfQuarter) + (numberOfDimes * valueOfDime) + (numberOfNickels * valueOfNickel));
		//System.out.println(totalMoneyFound);

        /*
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all?
        */
		int brierNumberOfMuffins = 18;
		int macadamsNumberOfMuffins = 20;
		int flanneryNumberOfMuffins = 17;
		int totalFirstGradeMuffins = brierNumberOfMuffins + flanneryNumberOfMuffins + macadamsNumberOfMuffins;
		//System.out.println(totalFirstGradeMuffins);
        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
		float priceOfYoyo = 0.24F;
		float priceOfWhistle = 0.14F;
		float totalCost = priceOfYoyo + priceOfWhistle;
		//System.out.println(totalCost);

        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
		int largeMarshmallows = 8;
		int smallMarshmallows = 10;
		int totalMarshmallows = largeMarshmallows + smallMarshmallows;


        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
		int hiltInchesOfSnow = 29;
		int schoolInchesOfSnow = 17;
		int hiltsSnowOverSchool = hiltInchesOfSnow - schoolInchesOfSnow;

        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?
        */
		float costOfTruck = 3.0f;
		float costOfPencil = 2.0f;
		float hiltTotalMoney = 10.00f;
		float hiltRemainingMoney = (hiltTotalMoney - (costOfPencil + costOfTruck));

        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
		int initialMarbles = 16;
		int lostMarbles = 7;
		int remainingMarbles = initialMarbles - lostMarbles;

        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
		int megansSeashells = 19;
		int goalAmountOfSeashells = 25;
		int amountOfSeashellsToGoal = goalAmountOfSeashells - megansSeashells;

        /*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
		int totalBalloons = 17;
		int redBalloons = 8;
		int greenBalloons = totalBalloons - redBalloons;

        /*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
		int initialBooksOnShelf = 38;
		int booksAddedToShelf = 10;
		int totalBooksOnShelf = initialBooksOnShelf + booksAddedToShelf;

        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
		int legsPerBee = 6;
		int totalNumberOfBees = 8;
		int totalLegsOfBees = legsPerBee * totalNumberOfBees;

        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
		float costOfIceCreamCone = 0.99f;
		int numberOfCones = 2;
		float totalCostOfCones = costOfIceCreamCone * numberOfCones;
		//System.out.println(totalCostOfCones);

        /*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
		int totalRocksForBorder = 125;
		int hiltsRocks = 64;
		int rocksNeededForBorder = totalRocksForBorder - hiltsRocks;

        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
		int hiltInitialMarbles = 38;
		int hiltLostMarbles = 15;
		int hiltRemainingMarbles = hiltInitialMarbles - hiltLostMarbles;

        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
		int totalMilesToConcert = 78;
		int milesDroveToGasStation = 32;
		int remainingMilesToConcert = totalMilesToConcert - milesDroveToGasStation;

        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?
        */
		int minutesShoveledSaturdayMorning = 90;
		int minutesShoveledSaturdayAfternoon = 45;
		int totalMinutesShoveledSaturday = minutesShoveledSaturdayAfternoon + minutesShoveledSaturdayMorning;

        /*
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
		int numHiltHotdogsPurcased = 6;
		float costPerHotdog = 0.50f;
		float totalCostOfHotdogs = numHiltHotdogsPurcased * costPerHotdog;

        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
		float hiltInitialMoney = 0.50f;
		float costPerPencil = 0.07f;
		int amountOfPencilsPurchasable = ((int)(hiltInitialMoney/costPerPencil));
		//System.out.println(amountOfPencilsPurchasable);

        /*
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
		int totalButterfliesSeen = 33;
		int orangeButterflies = 20;
		int redButterflies = totalButterfliesSeen - orangeButterflies;

        /*
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
		float moneyKateGaveClerk = 1.00f;
		float costPerCandy = 0.54f;
		float katesChange = moneyKateGaveClerk - costPerCandy;

        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
		int initialTreesInMarkYard = 13;
		int treesPlanted = 12;
		int totalTreesInMarkYard = initialTreesInMarkYard + treesPlanted;

        /*
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
		int hoursPerDay = 24;
		int daysUntilGrandma = 2;
		int totalHoursUntilGrandma = hoursPerDay * daysUntilGrandma;

        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
		int kimsNumberOfCousins = 4;
		int gumPerCousin = 5;
		int gumNeededForCousins = kimsNumberOfCousins * gumPerCousin;

        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
		float danInitialMoney = 3.00f;
		float costOfCandyBar = 1.00f;
		float danRemainingMoney = danInitialMoney - costOfCandyBar;

        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
		int numOfBoatsInLake = 5;
		int numOfPeoplePerBoat = 3;
		int totalNumPeopleInBoats = numOfBoatsInLake * numOfPeoplePerBoat;

        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
		int ellenInitialLegos = 380;
		int ellenLostLegos = 57;
		int ellenRemainingLegos = ellenInitialLegos - ellenLostLegos;

        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
		int initialMuffinsBaked = 35;
		int muffinGoalAmount = 85;
		int muffinsReaminingToBake = muffinGoalAmount - initialMuffinsBaked;

        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
		int willyTotalCrayons = 1400;
		int lucyTotalCrayons = 290;
		int moreCrayonsWillyHas = willyTotalCrayons - lucyTotalCrayons;

        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
		int stickersPerPage = 10;
		int totalPagesOfStickers = 22;
		int totalStickersInPages = stickersPerPage * totalPagesOfStickers;

        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
		int totalCupcakes = 96;
		int totalChildrenEatingCupcakes = 8;
		int cupcakesPerChild = totalCupcakes/totalChildrenEatingCupcakes;

        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
		int totalGingerbreadCookies = 47;
		int gingerbreadCookiesPerJar = 6;
		int gingerBreadCookiesRemaining = totalGingerbreadCookies % gingerbreadCookiesPerJar;
		//System.out.println(gingerBreadCookiesRemaining);

        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
		int totalCroissants = 59;
		int neighborsGettingCroissants = 8;
		int croissantsRemaining = totalCroissants %  neighborsGettingCroissants;
		//System.out.println(croissantsRemaining);

        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
		int totalCookiesToBake = 276;
		int cookiesPerTray = 12;
		int totalTraysNeeded = totalCookiesToBake/cookiesPerTray;

        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
		int totalPretzelsMade = 480;
		int servingSizeOfPretzels = 12;
		int totalServingsOfPretzels = totalPretzelsMade / servingSizeOfPretzels;

        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
		int totalCupcakesMade = 53;
		int cupcakesLeftHome = 2;
		int cupcakesPerBox = 3;
		int numOfCupcakeBoxes = ((totalCupcakesMade - cupcakesLeftHome) / cupcakesPerBox);
		//System.out.println(numOfCupcakeBoxes);

        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
		int carrotSticksPrepared = 74;
		int numPeopleEatingCarrot = 12;
		int remainingCarrotSticks = carrotSticksPrepared % numPeopleEatingCarrot;
		//System.out.println(remainingCarrotSticks);
        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
		int totalTeddyBears = 98;
		int bearsPerShelf = 7;
		int totalShelvesFull = totalTeddyBears / bearsPerShelf;

        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
		int totalFamilyPictures = 480;
		int picturesPerAlbum = 20;
		int totalAlbumsNeeded = totalFamilyPictures / picturesPerAlbum;

        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
		int totalNumCards = 94;
		int cardsPerBox = 8;
		int totalBoxesFilled = totalNumCards / cardsPerBox;
		int totalRemainingCards = totalNumCards % cardsPerBox;
		//System.out.println(totalBoxesFilled);
		//System.out.println(totalRemainingCards);

        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */
		int totalBooks = 210;
		int totalShelves = 10;
		int booksPerShelf = totalBooks / totalShelves;

        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
		int totalCroissantsBaked = 17;
		int totalGuestsGettingCroissants = 7;
		int totalCroissantsPerGuest = totalCroissantsBaked / totalGuestsGettingCroissants;
		//System.out.println(totalCroissantsPerGuest);

        /*
            CHALLENGE PROBLEMS
        */

        /*
        Bill and Jill are house painters. Bill can paint a 12 x 14 room in 2.15 hours, while Jill averages
        1.90 hours. How long will it take the two painter working together to paint 5 12 x 14 rooms?
        Hint: Calculate the hourly rate for each painter, combine them, and then divide the total walls in feet by the combined hourly rate of the painters.
        Challenge: How many days will it take the pair to paint 623 rooms assuming they work 8 hours a day?.
        */
		float billRoomRate = 2.15F;
		float jillRoomRate = 1.90f;
		int feetPerRoom = 12 * 14;
		float billHourlyRate = (feetPerRoom/billRoomRate);
		float jillHourlyRate = (feetPerRoom/jillRoomRate);
		float combinedHourlyRate = billHourlyRate + jillHourlyRate;
		int feetPerFiveRooms = 5 * feetPerRoom;
		float hoursToPaintFive = feetPerFiveRooms / combinedHourlyRate;
		System.out.println(hoursToPaintFive);

		int rooms = 623;
		int totalFeet = rooms * feetPerRoom;
		float totalHoursToPaint = totalFeet / combinedHourlyRate;
		int hoursPaintingPerDay = 8;
		float totalDaysToPaint = totalHoursToPaint / hoursPaintingPerDay;
		System.out.println(totalDaysToPaint);



        /*
        Create and assign variables to hold your first name, last name, and middle initial. Using concatenation,
        build an additional variable to hold your full name in the order of last name, first name, middle initial. The
        last and first names should be separated by a comma followed by a space, and the middle initial must end
        with a period.
        Example: "Hopper, Grace B."
        */
		String firstName = "Gabriel";
		String lastName = "Naples";
		char middleInitial = 'J';
		String fullName = lastName + ','+' '+firstName+' '+middleInitial+'.';
		System.out.println(fullName);


        /*
        The distance between New York and Chicago is 800 miles, and the train has already travelled 537 miles.
        What percentage of the trip has been completed?
        Hint: The percent completed is the miles already travelled divided by the total miles.
        Challenge: Display as an integer value between 0 and 100 using casts.
        */
		float totalDistance = 800.0f;
		float milesTraveled = 537.0f;
		float decimalNotation = milesTraveled/totalDistance;
		float percentageTripComplete = decimalNotation*100;
		System.out.println((int)percentageTripComplete);


	}

}
