package com.techelevator;

import com.techelevator.entity.TellerMachine;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Module1CodingAssessment {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Test Instantiation of An Object
		TellerMachine teller = new TellerMachine("NameOfMachine", BigDecimal.valueOf(100.00), BigDecimal.valueOf(75.00));
		//System.out.println(teller.getManufacturer() + " " + teller.getDeposits() + " " + teller.getWithdrawals() + " " + teller.getBalance());
		System.out.println(teller.toString() + "\n");

		//List that holds objects
		List<TellerMachine> tellerList = new ArrayList<>();

		//Read File Input and Create Objects --- Modularization would go in Biz
		File inputFile = new File("data-files/TellerInput.csv");
		try(Scanner fileScanner = new Scanner(inputFile)){
			while(fileScanner.hasNext()){
				String[] line = fileScanner.nextLine().split(",");
				TellerMachine newTeller = new TellerMachine(line[0], BigDecimal.valueOf(Float.parseFloat(line[1])), BigDecimal.valueOf(Float.parseFloat(line[2])));
				tellerList.add(newTeller);
			}
		} catch (FileNotFoundException ex){
			ex.getMessage();
		}

		//Get Total Balance --- Go in biz
		BigDecimal totalBalance = BigDecimal.ZERO;
		for(TellerMachine obj : tellerList){
			System.out.println(obj.toString());
			totalBalance = totalBalance.add(obj.getBalance());
		}

		System.out.println("Total Balance: " + totalBalance);

	}

}
