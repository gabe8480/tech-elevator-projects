package com.techelevator;
import java.util.Scanner;

public class LinearConvert {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Please enter the length: ");
		int lengthValue = Integer.parseInt(input.nextLine());
		System.out.println("Is the measurement in (m)eter, or (f)eet?");
		char typeValue = input.nextLine().charAt(0);
		int lengthConversion = lengthConvert(lengthValue, typeValue);

	}

	public static int lengthConvert(int val, char type) {
		float conversion = 0f;
		if (type == 'M' || type == 'm') {
			//f = m * 3.2808399
			conversion = Math.round(val * 3.2808399);
			System.out.println(val + Character.toString(type) + " is " + (int)conversion + "f");
		} else if (type == 'F' || type == 'f') {
			//m = f * 0.3048
			conversion = Math.round(val * 0.3048);
			//conversion = (float) ((float)val * 0.3048); //this outputs 17m which is the example
			System.out.println(val + Character.toString(type) + " is " + (int)conversion + "m");
		} else {
			System.out.println("Invalid Input.");
		}
		return (int) conversion;
	}
}
