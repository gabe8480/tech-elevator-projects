package com.techelevator;

import org.junit.*;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KataPotterTest {

    private KataPotter books;

    @Before
    public void setUp() throws Exception {
        books = new KataPotter();
    }
// THESE TESTS WERE FOR AN INCORRECT INPUT. THOUGHT BASKET EXAMPLE WAS THE INPUT
//    @Test
//    public void getCost_oneBook() {
//        assertEquals(8.0, books.getCost(new int[]{1}), 0.001);
//    }
//    @Test
//    public void getCost_twoDiffBooks() {
//        assertEquals(15.2, books.getCost(new int[]{1, 1}), 0.001);
//    }
//    @Test
//    public void getCost_twoSameBooks() {
//        assertEquals(16.0, books.getCost(new int[]{2}), 0.001);
//    }
//    @Test
//    public void getCost_twoSameBooks_oneDifferent() {
//        assertEquals(23.2, books.getCost(new int[]{2, 1}), 0.001);
//    }
//    @Test
//    public void getCost_ReadMe_Examples() {assertEquals(51.2, books.getCost(new int[]{2, 2, 2, 1, 1}), 0.001);}

    // TESTS FOR BOOK STREAM BEING TURNED INTO BASKET
    @Test
    public void getCost_oneBook() {
        assertEquals(8.0, books.getCost(new int[]{1}), 0.001);
    }
    @Test
    public void getCost_twoDiffBooks() {
        assertEquals(15.2, books.getCost(new int[]{1, 2}), 0.001);
    }
    @Test
    public void getCost_twoSameBooks() {
        assertEquals(16.0, books.getCost(new int[]{1,1}), 0.001);
    }
    @Test
    public void getCost_twoSameBooks_oneDifferent() {
        assertEquals(23.2, books.getCost(new int[]{1, 1, 2}), 0.001);
    }
    @Test
    public void getCost_ReadMe_Examples() {assertEquals(51.2, books.getCost(new int[]{1, 1, 2, 2, 3, 3, 4, 5}), 0.001);}
    @Test
    public void getCost_setFour_setOne() {assertEquals(33.6, books.getCost(new int[]{1, 2, 3, 4, 1}), 0.001);}
}
