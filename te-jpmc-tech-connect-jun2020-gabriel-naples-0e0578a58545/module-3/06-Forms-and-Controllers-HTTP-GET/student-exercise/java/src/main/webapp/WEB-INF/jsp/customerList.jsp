<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="All Customers List"/>

<%@include file="common/header.jspf"%>

<c:url var="formAction" value="/customerList" />
<form method="POST" action="${formAction}">
    <div>
        <label for="nameSearch">Actor First or Last Name</label>
        <input type="text" name="search" id="nameSearch" value=""/>
        <label for="sort">Sort</label>
        <select name="sort" id="sort">
            <!--three examples but make this fill dynamically with loop -->
            <option value="last_name">Last Name</option>
            <option value="email">Email</option>
            <option value="activebool">Active</option>
        </select>
        <input class="formSubmitButton" type="submit" value="Search Customers"/>
    </div>
</form>
<table class="table">
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Active</th>
    </tr>
    <c:forEach items="${customers}" var="customer">
        <tr>
            <td>
                    ${customer.firstName} ${customer.lastName}
            </td>
            <td>
                    ${customer.email}
            </td>
            <td>
                    <c:choose>
                        <c:when test="${customer.active}">
                            Yes
                        </c:when>
                        <c:otherwise>
                            No
                        </c:otherwise>
                    </c:choose>
            </td>
        </tr>
    </c:forEach>
</table>

<%@include file="common/footer.jspf"%>