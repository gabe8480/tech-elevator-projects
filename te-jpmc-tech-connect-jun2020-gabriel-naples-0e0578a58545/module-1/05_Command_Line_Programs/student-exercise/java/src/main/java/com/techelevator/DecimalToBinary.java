package com.techelevator;
import java.util.Scanner;

public class DecimalToBinary {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Please enter in a series of decimal values (separated by spaces): ");
		String binaryValue = input.nextLine();
		binaryConvert(binaryValue);

	}

	/* Original implementation
	public static void binaryConvert(String bin) {
		String num = "";
		for(int i = 0;i<bin.length();i++){
			if(bin.charAt(i) == ' '){
				System.out.println(num + " in binary is " + Integer.toBinaryString(Integer.parseInt(num)));
				num = "";
			} else if(i == bin.length()-1){
				num += bin.charAt(i);
				System.out.println(num + " in binary is " + Integer.toBinaryString(Integer.parseInt(num)));
			} else {
				num += bin.charAt(i);
				//System.out.println(num);
			}
		}

	}*/

	// adjusted implementation to do the actual math
	public static void binaryConvert(String bin) {
		String num = "";
		String binaryString = "";
		String correctOrder = "";
		int number = 0;
		for(int i = 0;i<bin.length();i++){
			if(bin.charAt(i) == ' '){
				number = Integer.parseInt(num);
				while(number >= 1){
					binaryString += String.valueOf(number % 2);
					number /= 2;
				}
				for(int j = binaryString.length()-1; j >= 0; j--){
					correctOrder += String.valueOf(binaryString.charAt(j));
				}
				System.out.println(num + " in binary is " + correctOrder);
				num = "";
				binaryString = "";
				correctOrder = "";
			} else if(i == bin.length()-1){
				num += bin.charAt(i);
				number = Integer.parseInt(num);
				while(number >= 1){
					binaryString += String.valueOf(number % 2);
					number /= 2;
				}
				for(int j = binaryString.length()-1; j >= 0; j--){
					correctOrder += String.valueOf(binaryString.charAt(j));
				}
				System.out.println(num + " in binary is " + correctOrder);
				num = "";
				binaryString = "";
				correctOrder = "";
			} else {
				num += bin.charAt(i);
				//System.out.println(num);
			}
		}

	}


}
