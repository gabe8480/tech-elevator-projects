<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Chase Java
  Date: 8/11/2020
  Time: 2:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" href="css/site.css"></link>
</head>
<body>
<h1>Squirrel Parties for Dummies!!</h1>
<c:url var="imgUrl" value="/img/forDummies.png"/>
<c:url var="starImg" value="/img/star.png"/>
<img src="${imgUrl}">
<h3>Reviews</h3>
<h4><a href="reviewInput">Write A Review</a></h4>

    <c:forEach items="${reviews}" var="review">
        <div id="reviewData">
            <table>
            <tr>
                <td><b>${review.title}</b> (${review.username})</td>
            </tr>
            <tr>
                <td>${localDateTimeFormat.format(review.dateSubmitted)}</td>
            </tr>
            <tr>
                <td>
                        <c:forEach begin="0" end="${review.rating}" var="i">
                            <img src="${starImg}"/>
                        </c:forEach>
                </td>
            </tr>
            <tr>
                <td>${review.text}</td>
            </tr>
            </table>
        </div>
    </c:forEach>


</body>
</html>
