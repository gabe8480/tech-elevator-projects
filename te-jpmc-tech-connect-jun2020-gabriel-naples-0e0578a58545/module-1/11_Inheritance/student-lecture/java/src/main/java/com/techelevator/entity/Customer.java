package com.techelevator.entity;

import java.time.LocalDate;

public class Customer extends Person {
    /*
        Define fields of the class - usually set their scope to private (only the methods in this class)
     */
    private static int nextId = 1;

    public static int getNextId(){
        return nextId;
    }

    private int id; // unique id we generate for all customers

    private LocalDate birthDate;

    public Customer(){
        this(null, null, null);
    };

    public Customer(String name, String email, LocalDate birthDate) {
        super(name, email);// must be the first line
        this.id = nextId;
        nextId++;
        this.birthDate = birthDate;
    }

    public int getId() {
        return id;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", birthDate=" + birthDate +
                '}';
    }
}
