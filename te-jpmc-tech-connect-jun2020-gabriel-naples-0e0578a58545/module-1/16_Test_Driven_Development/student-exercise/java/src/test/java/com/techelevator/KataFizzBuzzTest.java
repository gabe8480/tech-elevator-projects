package com.techelevator;

import org.junit.*;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KataFizzBuzzTest {
    private KataFizzBuzz fizz;

    @Before
    public void setUp() throws Exception {
        fizz = new KataFizzBuzz();
    }
    @Test
    public void fizzBuzz_divisible_by_3(){
        assertEquals("Fizz", fizz.fizzBuzz(3));
    }
    @Test
    public void fizzBuzz_divisible_by_5(){
        assertEquals("Buzz", fizz.fizzBuzz(5));
    }
    @Test
    public void fizzBuzz_divisible_by_5_and_3(){
        assertEquals("FizzBuzz", fizz.fizzBuzz(15));
    }
    @Test
    public void fizzBuzz_under_100(){
        assertEquals("97", fizz.fizzBuzz(97));
    }
    @Test
    public void fizzBuzz_over_100(){
        assertEquals("", fizz.fizzBuzz(101));
    }
    @Test
    public void fizzBuzz_contains_3_and_5(){
        assertEquals("FizzBuzz", fizz.fizzBuzz(53));
    }
    @Test
    public void fizzBuzz_contains_3(){
        assertEquals("Fizz", fizz.fizzBuzz(13));
    }
    @Test
    public void fizzBuzz_contains_5(){
        assertEquals("Buzz", fizz.fizzBuzz(52));
    }
}
