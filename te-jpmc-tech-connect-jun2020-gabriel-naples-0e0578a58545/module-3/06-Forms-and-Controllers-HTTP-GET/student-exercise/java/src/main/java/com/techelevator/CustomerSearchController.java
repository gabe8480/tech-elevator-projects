package com.techelevator;

import com.techelevator.dao.CustomerDao;
import com.techelevator.dao.model.Customer;
import com.techelevator.dao.model.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class CustomerSearchController {

    @Autowired
    private CustomerDao customerDao;

    @RequestMapping("/customerList")
    public String showCustomerSearchForm() {
        return "customerList";
    }

    @RequestMapping(value = "/customerList", method = RequestMethod.POST)
    public String searchCustomers(@RequestParam String search, @RequestParam String sort, ModelMap modelHolder) {
        List<Customer> customers = customerDao.searchAndSortCustomers(search, sort);
        modelHolder.put("customers", customers);
        return "customerList";
    }

}