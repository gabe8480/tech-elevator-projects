<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp">
    <c:param name="title" value="Favorite Color" />
</c:import>

<c:set var="favorites" value="${favorites}"/>

<p><b>Favorite Food:</b> ${favorites.favoriteFood}</p>
<p><b>Favorite Color:</b> ${favorites.favoriteColor}</p>
<p><b>Favorite Season:</b> ${favorites.favoriteSeason}</p>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />