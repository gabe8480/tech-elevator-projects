package com.techelevator;

import com.techelevator.dao.FilmDao;
import com.techelevator.dao.model.Film;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * FilmSearchController
 */
@Controller
public class FilmSearchController {

    @Autowired
    FilmDao filmDao;

    @RequestMapping("/filmList")
    public String showFilmSearchForm(ModelMap modelHolder) {
        modelHolder.put("genres", getGenres());
        return "filmList";
    }

    @RequestMapping(value = "/filmList", method = RequestMethod.POST)
    public String searchFilms(@RequestParam String genre, @RequestParam int minLength, @RequestParam int maxLength, ModelMap modelHolder) {
        List<Film> films = filmDao.getFilmsBetween(genre, minLength, maxLength);
        modelHolder.put("genres", getGenres());
        modelHolder.put("films", films);
        return "filmList";
    }

    private List<String> getGenres(){
        List<String> genres = filmDao.getAllGenres();
        return genres;
    }

}