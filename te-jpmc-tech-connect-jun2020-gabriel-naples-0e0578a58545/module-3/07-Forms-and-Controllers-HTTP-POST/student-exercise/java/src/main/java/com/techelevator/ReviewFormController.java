package com.techelevator;

import com.techelevator.model.Review;
import com.techelevator.model.ReviewDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDateTime;

@Controller
public class ReviewFormController {

    @Autowired
    ReviewDao reviewDao;

    @RequestMapping(value = "/reviewInput", method = RequestMethod.GET)
    public String displayReviewForm(){
        return "reviewInput";
    }

    @RequestMapping(value = "/reviewInput", method = RequestMethod.POST)
    public String submitReviewForm(Review review){
        review.setDateSubmitted(LocalDateTime.now());
        reviewDao.save(review);
        return "redirect:/";
    }


}
