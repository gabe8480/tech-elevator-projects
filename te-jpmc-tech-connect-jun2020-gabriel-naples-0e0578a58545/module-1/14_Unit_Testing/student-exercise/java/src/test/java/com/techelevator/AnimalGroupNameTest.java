package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AnimalGroupNameTest {

    private AnimalGroupName animal;

    @Before
    public void setUp() throws Exception {
        animal = new AnimalGroupName();
    }

    @Test
    public void getHerdAllUpper() {
        assertEquals("Herd", animal.getHerd("DEER"));
    }

    @Test
    public void getHerdAllLower() {
        assertEquals("Kit", animal.getHerd("pigeon"));
    }

    @Test
    public void getHerdNull() {
        assertEquals("unknown", animal.getHerd(""));
    }

    @Test
    public void getHerdAnimalNotPresent() {
        assertEquals("unknown", animal.getHerd("pig"));
    }


}