<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="All Actors List"/>

<%@include file="common/header.jspf"%>
<!-- Form goes here -->
<c:url var="formAction" value="/actorList" />
<form method="POST" action="${formAction}">
    <div>
        <label for="search">Name</label>
        <input type="text" name="search" id="search" value=""/>
        <input class="formSubmitButton" type="submit" value="Search Actor"/>
    </div>
</form>
<table class="table">
<tr>
<th>Name</th>
</tr>
<c:forEach items="${actors}" var="actor">
<tr>
    <td>
        ${actor.firstName} ${actor.lastName}
    </td>
</tr>
</c:forEach>
</table>
<%@include file="common/footer.jspf"%>