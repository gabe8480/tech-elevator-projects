--DROP TABLE projects;
--DROP TABLE departments;
--DROP TABLE jobs;
--DROP TABLE employees;
--DROP TABLE projects_employees;

BEGIN TRANSACTION;

CREATE TABLE projects (
        project_number          SERIAL          PRIMARY KEY,
        name                    VARCHAR(45)     NOT NULL,
        start_date              DATE            NOT NULL DEFAULT CURRENT_DATE        
);

CREATE TABLE departments (
        department_number       SERIAL          PRIMARY KEY,
        name                    VARCHAR(45)     UNIQUE NOT NULL
);

CREATE TABLE jobs (
    job_number          SERIAL          PRIMARY KEY,
    name                VARCHAR(45)     UNIQUE NOT NULL    
);

CREATE TABLE employees (
        employee_id     SERIAL          PRIMARY KEY,
        job_id          INTEGER         NOT NULL,
        last_name       VARCHAR(45)     NOT NULL,
        first_name      VARCHAR(45)     NOT NULL,
        gender          CHARACTER(1)    NULL,
        date_of_birth   DATE            NOT NULL,
        date_of_hire    DATE            NOT NULL,
        department_id   INTEGER         NOT NULL,
        
        CONSTRAINT fk_job_id FOREIGN KEY (job_id) REFERENCES jobs(job_number),
        CONSTRAINT fk_department_id FOREIGN KEY (department_id) REFERENCES departments (department_number)
);

--This table is exists in case employees can be put on multiple projects
CREATE TABLE projects_employees(
        project_number          INTEGER         NOT NULL,
        employee_number         INTEGER         NOT NULL,
        
        CONSTRAINT fk_project_number FOREIGN KEY (project_number) REFERENCES projects (project_number),
        CONSTRAINT fk_employee_number FOREIGN KEY (employee_number) REFERENCES employees (employee_id)

);

COMMIT;


INSERT INTO departments (name) VALUES ('Sales');
INSERT INTO departments (name) VALUES ('Human Resources');
INSERT INTO departments (name) VALUES ('IT');

INSERT INTO projects (name, start_date) VALUES ('FORMAT LAPTOPS', '2000-05-05');
INSERT INTO projects (name, start_date) VALUES ('UPDATE TIMESHEETS', '2010-10-01');
INSERT INTO projects (name, start_date) VALUES ('FORMAT SALES SHEETS', '2020-05-05');
INSERT INTO projects (name, start_date) VALUES ('FIRE PAM', '2020-07-25');

INSERT INTO jobs (name) VALUES ('Sales Associate');
INSERT INTO jobs (name) VALUES ('Software Developer');
INSERT INTO jobs (name) VALUES ('Hiring Manager');
INSERT INTO jobs (name) VALUES ('Associate');
INSERT INTO jobs (name) VALUES ('Vice President');

INSERT INTO employees (job_id, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES (1, 'John', 'Hankcock', 'M', '1984-01-12', '2000-01-01', 1);
INSERT INTO employees (job_id, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES (2, 'Gabe', 'Naples', 'M', '1998-05-05', '2000-01-05', 3);
INSERT INTO employees (job_id, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES (5, 'Paul', 'Forrest', 'M', '1900-01-12', '2010-01-01', 2);
INSERT INTO employees (job_id, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES (4, 'Jack', 'Harty', 'M', '1994-01-12', '2018-01-01', 1);   
INSERT INTO employees (job_id, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES (2, 'Brenden', 'Sosnader', 'M', '1989-01-08', '2050-01-01', 3);
INSERT INTO employees (job_id, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES (1, 'Joe', 'Brindisi', 'M', '2000-06-05', '2000-01-05', 2);
INSERT INTO employees (job_id, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES (3, 'Nikki', 'Deniro', 'F', '1999-01-12', '2011-01-01', 1);
INSERT INTO employees (job_id, first_name, last_name, gender, date_of_birth, date_of_hire, department_id)
        VALUES (5, 'Monica', 'Hacksy', 'F', '1997-12-12', '2012-01-01', 1);
        
INSERT INTO projects_employees (project_number, employee_number) VALUES(1, 4);
INSERT INTO projects_employees (project_number, employee_number) VALUES(2, 1);
INSERT INTO projects_employees (project_number, employee_number) VALUES(3, 2);
INSERT INTO projects_employees (project_number, employee_number) VALUES(4, 3);
INSERT INTO projects_employees (project_number, employee_number) VALUES(1, 1);
