package com.techelevator.biz;

import com.techelevator.data.TellerMachinesList;
import com.techelevator.entity.TellerMachine;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TellerMachineDAOFile {

    private String inputFile;

    public  TellerMachineDAOFile(){}

    public TellerMachineDAOFile(String inputFile){
        this.inputFile = inputFile;
    }

    //Starting to modularize
    public TellerMachinesList generateTellerList(){
        List<TellerMachine> tellerList = new ArrayList<>();
        File file = new File(inputFile);
        try(Scanner fileScanner = new Scanner(file)){
            while(fileScanner.hasNext()){
                String[] line = fileScanner.nextLine().split(",");
                TellerMachine newTeller = new TellerMachine(line[0], BigDecimal.valueOf(Float.parseFloat(line[1])), BigDecimal.valueOf(Float.parseFloat(line[2])));
                tellerList.add(newTeller);
            }
        } catch (FileNotFoundException ex){
            ex.getMessage();
        }
        TellerMachinesList tellerMachineList = new TellerMachinesList(tellerList);
        return tellerMachineList;
    }

}
