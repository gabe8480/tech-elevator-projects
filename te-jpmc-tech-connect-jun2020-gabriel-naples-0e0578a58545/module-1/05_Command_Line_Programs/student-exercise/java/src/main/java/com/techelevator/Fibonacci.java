package com.techelevator;
import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Please enter the Fibonacci number: ");
		int endValue = Integer.parseInt(input.nextLine());
		fibPrinter(endValue);

	}

	public static void fibPrinter(int endVal){
		int valTwoBefore = 0, valOneBefore = 0, nextFibCalculation = 1; // temporary storage for numbers in sequence
		while(nextFibCalculation <= endVal) // operates two steps ahead of line printing
		{
			valTwoBefore = valOneBefore; //two digits back from calculated new val
			valOneBefore = nextFibCalculation;// one digit back from calculated
			nextFibCalculation = valTwoBefore + valOneBefore;//calculated
			System.out.print(valTwoBefore+", ");//prints two digits back from calculation because previous 2 digits are needed for calc
		}
		System.out.print(valOneBefore); // because c being larger than end value stops loop, prints the last value before cutoff.
	}

}
