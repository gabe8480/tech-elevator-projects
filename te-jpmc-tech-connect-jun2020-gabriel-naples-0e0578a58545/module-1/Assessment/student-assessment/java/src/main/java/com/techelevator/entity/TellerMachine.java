package com.techelevator.entity;

import java.math.BigDecimal;

public class TellerMachine {

    private String manufacturer;
    private BigDecimal deposits;
    private BigDecimal withdrawals;

    public TellerMachine(){}

    public TellerMachine(String manufacturer, BigDecimal deposits, BigDecimal withdrawals){
        this.manufacturer = manufacturer;
        this. deposits = deposits;
        this.withdrawals = withdrawals;
    }

    //Setters and Getters
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public BigDecimal getDeposits() {
        return deposits;
    }

    public void setDeposits(BigDecimal deposits) {
        this.deposits = deposits;
    }

    public BigDecimal getWithdrawals() {
        return withdrawals;
    }

    public void setWithdrawals(BigDecimal withdrawals) {
        this.withdrawals = withdrawals;
    }

    public BigDecimal getBalance(){
        BigDecimal balance = deposits.subtract(withdrawals);
        return balance;
    }

    //Methods

    public boolean validateCardNumber(String cardNumber){
        if(cardNumber.charAt(0) == '5' && cardNumber.length() == 16){
            return true;
        } else if (cardNumber.charAt(0) == '4' && (cardNumber.length() == 13 || cardNumber.length() == 16)){
            return true;
        } else if(cardNumber.charAt(0) == '3' && (cardNumber.charAt(1) == '4' || cardNumber.charAt(1) == '7')){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "ATM - " + manufacturer + " - " + getBalance();
    }
}
