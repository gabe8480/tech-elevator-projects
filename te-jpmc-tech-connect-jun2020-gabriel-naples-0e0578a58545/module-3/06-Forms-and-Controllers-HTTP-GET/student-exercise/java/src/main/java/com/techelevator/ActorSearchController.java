package com.techelevator;

import java.util.List;
import java.util.Map;

import com.techelevator.dao.ActorDao;

import com.techelevator.dao.model.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ActorSearchController {

	@Autowired
	private ActorDao actorDao;

	/* What request mapping do we want here */
	@RequestMapping("/actorList")
	public String showSearchActorForm() {
		return "actorList";
	}

	/* What about here? */
	@RequestMapping(value = "/actorList", method = RequestMethod.POST)
	public String searchActors(@RequestParam String search, ModelMap modelHolder) {
		/* Call the model and pass values to the jsp */
		List<Actor> actors = actorDao.getMatchingActors(search);
		modelHolder.put("actors", actors);
		return "actorList";
	}
}
