<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="All Films List"/>

<%@include file="common/header.jspf"%>

<c:url var="formAction" value="/filmList" />
<form method="POST" action="${formAction}">
    <div>
        <label for="minLength">Minimum Length</label>
        <input type="text" name="minLength" id="minLength" value="0"/>
        <label for="maxLength">Maximum Length</label>
        <input type="text" name="maxLength" id="maxLength" value="0"/>
        <label for="genre">Genre</label>
        <select name="genre" id="genre">
            <c:forEach items="${genres}" var="genre">
                <option value="${genre}">${genre}</option>
            </c:forEach>
            <!--three examples but make this fill dynamically with loop -->
            <%--<option value="Action">Action</option>
            <option value="Animation">Animation</option>
            <option value="Comedy">Comedy</option>--%>
        </select>
        <input class="formSubmitButton" type="submit" value="Search Films"/>
    </div>
</form>
<table class="table">
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Release Year</th>
        <th>Length</th>
        <th>Rating</th>
    </tr>
    <c:forEach items="${films}" var="film">
        <tr>
            <td>
                ${film.title}
            </td>
            <td>
                    ${film.description}
            </td>
            <td>
                    ${film.releaseYear}
            </td>
            <td>
                    ${film.length}
            </td>
            <td>
                    ${film.rating}
            </td>
        </tr>
    </c:forEach>
</table>

<%@include file="common/footer.jspf"%>