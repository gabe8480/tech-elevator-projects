package com.techelevator;

public class Airplane {

    private String planeNumber;
    private int bookedFirstClassSeats;
    private int availableFirstClassSeats;
    private int totalFirstClassSeats;
    private int bookedCoachSeats;
    private int availableCoachSeats;
    private int totalCoachSeats;

    //constructor
    public Airplane(String planeNumber, int totalFirstClassSeats, int totalCoachSeats){
        //plane info
        this.planeNumber = planeNumber;
        //first class
        this.totalFirstClassSeats = totalFirstClassSeats;
        this.availableFirstClassSeats = totalFirstClassSeats;
        this.bookedFirstClassSeats = 0;
        //coach
        this.totalCoachSeats = totalCoachSeats;
        this.availableCoachSeats = totalCoachSeats;
        this.bookedCoachSeats = 0;
    }

    //Getters
    public String getPlaneNumber() {
        return planeNumber;
    }

    public int getBookedFirstClassSeats() {
        return bookedFirstClassSeats;
    }

    public int getAvailableFirstClassSeats() {
        return availableFirstClassSeats;
    }

    public int getTotalFirstClassSeats() {
        return totalFirstClassSeats;
    }

    public int getBookedCoachSeats() {
        return bookedCoachSeats;
    }

    public int getAvailableCoachSeats() {
        return availableCoachSeats;
    }

    public int getTotalCoachSeats() {
        return totalCoachSeats;
    }

    //Methods
    public boolean reserveSeats(boolean forFirstClass, int totalNumberOfSeats){
        if(forFirstClass && totalNumberOfSeats < this.availableFirstClassSeats){
            this.bookedFirstClassSeats += totalNumberOfSeats;
            this.availableFirstClassSeats -= totalNumberOfSeats;
            return true;
        } else if (!forFirstClass && totalNumberOfSeats < this.availableCoachSeats){
            this.bookedCoachSeats += totalNumberOfSeats;
            this.availableCoachSeats -= totalNumberOfSeats;
            return true;
        }
        return false;
    }
}
