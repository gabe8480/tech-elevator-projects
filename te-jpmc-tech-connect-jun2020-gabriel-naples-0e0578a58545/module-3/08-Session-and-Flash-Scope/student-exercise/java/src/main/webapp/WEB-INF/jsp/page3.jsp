<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp">
    <c:param name="title" value="Favorite Season" />
</c:import>

<form method="POST">
    <label>What is your favorite season?</label>
    <%--<select name="favoriteSeason">
        <option value="Spring">Spring</option>
        <option value="Summer">Summer</option>
        <option value="Fall">Fall</option>
        <option value="Winter">Winter</option>
    </select>--%>
    <div class="choice">
        <label for="Spring">
            <input type="radio" name="favoriteSeason" id="Spring" value="Spring"/>Spring
        </label>

        <label for="Summer"><input type="radio" name="favoriteSeason" id="Summer" value="Summer"/>Summer</label>

        <label for="Fall"><input type="radio" name="favoriteSeason" id="Fall" value="Fall"/>Fall</label>

        <label for="Winter"><input type="radio" name="favoriteSeason" id="Winter" value="Winter"/>Winter</label>
    </div>

    <button type="submit">Next >>></button>
</form>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />