package com.techelevator;

public class Elevator {

    private int currentFloor;
    private int numberOfFloors;
    private boolean doorOpen;

    //Constructor
    public Elevator(int totalNumberOfFloors){
        this.numberOfFloors = totalNumberOfFloors;
        this.currentFloor = 1;
    }

    //Getters
    public int getCurrentFloor() {
        return currentFloor;
    }

    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public boolean isDoorOpen() {
        return doorOpen;
    }

    public void openDoor(){
        this.doorOpen = true;
    }

    public void closeDoor(){
        this.doorOpen = false;
    }

    public void goUp(int desiredFloor){
        if(!doorOpen && desiredFloor <= numberOfFloors && desiredFloor > currentFloor){
            currentFloor = desiredFloor;
        }
    }

    public void goDown(int desiredFloor){
        if(!doorOpen && desiredFloor > 0 && desiredFloor < currentFloor){
            currentFloor = desiredFloor;
        }
    }

}
