package com.techelevator.dog;

public class Dog {

	private boolean isSleeping;


	public Dog(){
	    this.isSleeping = false;
    }

    public Dog(boolean isSleeping){
	    this.isSleeping = isSleeping;
    }

    //Getter. I initially was very confused here because i had getIsSleeping as the name. If there is only one getter should
    //the name of the method not start with 'get'?
    public boolean isSleeping(){
        return isSleeping;
    }

    //methods
    public String makeSound(){
        if(isSleeping){
            return "Zzzzz...";
        }
        return "Woof!";
    }

    public void sleep(){
        isSleeping = true;
    }

    public void wakeUp(){
        isSleeping = false;
    }

}
