package com.techelevator.validation.model;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class Registration {

    @Size(max = 20, message = "Must be under 20 characters")
    private String firstName;

    @Size(max = 20, message = "Must be under 20 characters")
    private String lastName;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Email
    private String confirmEmail;

    private Boolean emailMatching;

    @NotBlank
    @Size(min = 8, message = "password must be at least 8 characters")
    private String password;

    @NotBlank
    @Size(min = 8, message = "password must be at least 8 characters")
    private String confirmPassword;

    private Boolean passwordMatching;

    @Pattern(regexp = "^\\d{2}\\/\\d{2}\\/\\d{4}$", message = "not valid birthday")
    private String birthDate;

    @Min(value = 1, message = "valid range 1-10")
    @Max(value = 10, message = "valid range 1-10")
    private int numberOfTickets;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmEmail() {
        return confirmEmail;
    }

    public void setConfirmEmail(String confirmEmail) {
        this.confirmEmail = confirmEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(int numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public Boolean getEmailMatching() {
        return emailMatching;
    }

    public void setEmailMatching(Boolean emailMatching) {
        this.emailMatching = emailMatching;
    }

    public Boolean getPasswordMatching() {
        return passwordMatching;
    }

    public void setPasswordMatching(Boolean passwordMatching) {
        this.passwordMatching = passwordMatching;
    }

    @AssertTrue(message = "passwords must match")
    public boolean isPasswordMatching(){
        if(password != null){
            return password.equals(confirmPassword);
        }
        return true;
    }


    @AssertTrue(message = "emails must match")
    public boolean isEmailMatching(){
        if(email != null){
            return email.equals(confirmEmail);
        }
        return true;
    }

}
