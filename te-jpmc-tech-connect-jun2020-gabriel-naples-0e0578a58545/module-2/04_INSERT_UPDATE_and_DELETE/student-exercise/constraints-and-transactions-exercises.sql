-- Write queries to return the following:
-- The following changes are applied to the "dvdstore" database.**

-- 1. Add actors, Hampton Avenue, and Lisa Byway to the actor table.
INSERT INTO actor (first_name, last_name) VALUES ('HAMPTON','AVENUE');
INSERT INTO actor (first_name, last_name) VALUES ('LISA','BYWAY');
--SELECT * FROM actor WHERE first_name = 'HAMPTON' AND last_name = 'AVENUE';

-- 2. Add "Euclidean PI", "The epic story of Euclid as a pizza delivery boy in
-- ancient Greece", to the film table. The movie was released in 2008 in English.
-- Since its an epic, the run length is 3hrs and 18mins. There are no special
-- features, the film speaks for itself, and doesn't need any gimmicks.
INSERT INTO film (title, description, release_year, language_id, length) 
        VALUES('Euclidean PI', 'The epic story of Euclid as a pizza delivery boy in
-- ancient Greece', 2008, 1, 198);
SELECT * FROM film WHERE title = 'Euclidean PI';

-- 3. Hampton Avenue plays Euclid, while Lisa Byway plays his slightly
-- overprotective mother, in the film, "Euclidean PI". Add them to the film.
SELECT * FROM actor WHERE first_name = 'HAMPTON' AND last_name = 'AVENUE' OR first_name = 'LISA' AND last_name = 'BYWAY';
INSERT INTO film_actor (film_id, actor_id) VALUES (1001, 201);
INSERT INTO film_actor (film_id, actor_id) VALUES (1001, 202);
--Above Two INSERTS work or the below two 
INSERT INTO film_actor (film_id, actor_id) 
VALUES((SELECT film_id FROM film WHERE film.title = 'Euclidean PI'), (SELECT actor_id FROM actor WHERE actor.first_name = 'HAMPTON' AND actor.last_name = 'AVENUE'));
INSERT INTO film_actor (film_id, actor_id) 
VALUES((SELECT film_id FROM film WHERE film.title = 'Euclidean PI'), (SELECT actor_id FROM actor WHERE actor.first_name = 'LISA' AND actor.last_name = 'BYWAY'));

SELECT * FROM film_actor WHERE actor_id = 201 OR actor_id = 202;


-- 4. Add Mathmagical to the category table.
INSERT INTO category (name) VALUES ('Mathmagical');
SELECT * FROM category WHERE name = 'Mathmagical';


-- 5. Assign the Mathmagical category to the following films, "Euclidean PI",
-- "EGG IGBY", "KARATE MOON", "RANDOM GO", and "YOUNG LANGUAGE"
SELECT film_id FROM film WHERE title = 'Euclidean PI' OR title = 'EGG IGBY' OR title = 'RANDOM GO' OR title = 'KARATE MOON' OR title = 'YOUNG LANGUAGE';
INSERT INTO film_category (film_id, category_id) VALUES (274, 17);
INSERT INTO film_category (film_id, category_id) VALUES (494, 17);
INSERT INTO film_category (film_id, category_id) VALUES (714, 17);
INSERT INTO film_category (film_id, category_id) VALUES (996, 17);
INSERT INTO film_category (film_id, category_id) VALUES (1001, 17);
--above or below work. I went back and tested the bottom version after doing the top to see other ways of doing things.
INSERT INTO film_category (film_id, category_id) VALUES ((SELECT film_id FROM film WHERE film.title = 'Euclidean PI'), (SELECT category_id FROM category WHERE category.name = 'Mathmagical'));
INSERT INTO film_category (film_id, category_id) VALUES ((SELECT film_id FROM film WHERE film.title = 'EGG IGBY'), (SELECT category_id FROM category WHERE category.name = 'Mathmagical'));
INSERT INTO film_category (film_id, category_id) VALUES ((SELECT film_id FROM film WHERE film.title = 'RANDOM GO'), (SELECT category_id FROM category WHERE category.name = 'Mathmagical'));
INSERT INTO film_category (film_id, category_id) VALUES ((SELECT film_id FROM film WHERE film.title = 'KARATE MOON'), (SELECT category_id FROM category WHERE category.name = 'Mathmagical'));
INSERT INTO film_category (film_id, category_id) VALUES ((SELECT film_id FROM film WHERE film.title = 'YOUNG LANGUAGE'), (SELECT category_id FROM category WHERE category.name = 'Mathmagical'));

SELECT * FROM film_category WHERE film_id = 274 OR film_id = 494 OR film_id = 714 OR film_id = 996 OR film_id = 1001;

-- 6. Mathmagical films always have a "G" rating, adjust all Mathmagical films
-- accordingly.
-- (5 rows affected)
SELECT rating, title 
FROM film
JOIN film_category ON film_category.film_id = film.film_id
WHERE film_category.category_id = 17;  

BEGIN TRANSACTION;
UPDATE film 
SET rating = 'G'
FROM film_category
WHERE film.film_id = film_category.film_id AND film_category.category_name = 'Mathmagical'; 
COMMIT;

-- 7. Add a copy of "Euclidean PI" to all the stores.
SELECT *
FROM inventory
WHERE film_id = 1001--or(SELECT film_id FROM film WHERE title = 'Euclidean PI');

SELECT * FROM store;

INSERT INTO inventory (film_id, store_id) VALUES (1001, 1);
INSERT INTO inventory (film_id, store_id) VALUES (1001, 2);

-- 8. The Feds have stepped in and have impounded all copies of the pirated film,
-- "Euclidean PI". The film has been seized from all stores, and needs to be
-- deleted from the film table. Delete "Euclidean PI" from the film table.
DELETE FROM film WHERE title = 'Euclidean PI' AND film_id = 1001;
-- (Did it succeed? Why?)
-- It failed because of foreign key constraints. The film_actor table uses the PK film_id from film to match with the actor_id of the actors in the film, since two actors were in the film.

-- 9. Delete Mathmagical from the category table.
-- (Did it succeed? Why?)
DELETE FROM category WHERE name = 'Mathmagical';
--It failed because of foreign key constraints. The film_category table relies on the PK category_id from category for the Mathmagical entry since multiple films are referencing that category.

-- 10. Delete all links to Mathmagical in the film_category tale.
-- (Did it succeed? Why?)
Select * FROM category;
DELETE FROM film_category WHERE category_id = 17;
-- It succeeded because it is deleting linked references to primary keys of other tables. Deleting this linked reference does not interfere with the overall integrity of other tables.

-- 11. Retry deleting Mathmagical from the category table, followed by retrying
-- to delete "Euclidean PI".
DELETE FROM category WHERE name = 'Mathmagical';
DELETE FROM film WHERE title = 'Euclidean PI' AND film_id = 1001;
-- (Did either deletes succeed? Why?)
-- Deleting Mathmagical from the category table succeeded but deleting Euclidean PI from the film table failed. The first delete succeeded because no other table is relying on a reference
-- to the name and PK in category for Mathmagical. However, the film_actor table is still relying on the film_id from film so deleting that would mess up the integrity of the film_actor table.

-- 12. Check database metadata to determine all constraints of the film id, and
-- describe any remaining adjustments needed before the film "Euclidean PI" can
-- be removed from the film table.
-- film_id still has a foreign key constraint in both the film_actor table and the inventory table. The reference to the film_id associated with Euclidean PI would have to be deleted from both the
-- and film_actor tables before the film is able to be deleted from the film table. 
