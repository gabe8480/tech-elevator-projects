package com.techelevator.company;

public class Company {

    private String name;
    private int numberOfEmployees;
    private double revenue;
    private double expenses;

    public Company(){

    }

    //setter and getter methods
    public void setName(String name){
        this.name = name;
    }
    public void setNumberOfEmployees(int numberOfEmployees){
        this.numberOfEmployees = numberOfEmployees;
    }
    public void setRevenue(double revenue){
        this.revenue = revenue;
    }
    public void setExpenses(double expenses){
        this.expenses = expenses;
    }
    public int getNumberOfEmployees() {
        return numberOfEmployees;
    }
    public String getName() {
        return name;
    }
    public double getRevenue() {
        return revenue;
    }
    public double getExpenses() {
        return expenses;
    }

    //Methods
    public String getCompanySize(){
	    if(numberOfEmployees <= 50){
	        return "small";
        } else if (numberOfEmployees > 50 && numberOfEmployees <= 250){
            return "medium";
        }else if(numberOfEmployees > 250) {
            return "large";
        } else {
	        return "no negative nums";
        }
    }

    public double getProfit(){
	    return (revenue - expenses);
    }
	
}
