package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordSearch {

	public static void main(String[] args) {
		int lineCounter = 0;

		Scanner userInput = new Scanner(System.in);
		System.out.println("What is the file that should be searched?");
		String fileName = userInput.nextLine();
		System.out.println("What is the search word you are looking for?");
		String searchWord = userInput.nextLine();
		System.out.println("Should the search be case sensitive? (Y\\N)");
		String caseSensitive = userInput.nextLine();

		try (Scanner searchFileScanner = new Scanner(new File(fileName));){
			while(searchFileScanner.hasNext()){
				lineCounter++;
				String storyLine = searchFileScanner.nextLine();
				String actualString = storyLine;
				if(caseSensitive.equals("N")){
					storyLine = storyLine.toLowerCase();
					searchWord = searchWord.toLowerCase();
				}
				if(storyLine.contains(searchWord)){
					System.out.println(lineCounter + ") " + actualString);
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}


	}

}
