package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class WordCountTest {

    private WordCount word;

    @Before
    public void setUp() throws Exception {
        word = new WordCount();
    }

    @Test
    public void getCountAcceptedValues() {
        String[] entered = new String[]{"H", "H", "Brother"};
        Map<String, Integer> actual = new HashMap<>();
        actual.put("H", 2);
        actual.put("Brother", 1);
        assertEquals(actual, word.getCount(entered));
    }
    @Test
    public void getCountOneOfEach() {
        String[] entered = new String[]{"a", "b", "c"};
        Map<String, Integer> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", 1);
        actual.put("c", 1);
        assertEquals(actual, word.getCount(entered));
    }

    @Test
    public void getCountNull() {
        String[] entered = new String[]{};
        Map<String, Integer> actual = new HashMap<>();
        assertEquals(actual, word.getCount(entered));
    }

}