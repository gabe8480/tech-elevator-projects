package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NonStartTest {

    NonStart non;

    @Before
    public void setUp() throws Exception {
        non = new NonStart();
    }

    @Test
    public void getPartialStringOneCharEach() {
        assertEquals("", non.getPartialString("h", "h"));
    }

    @Test
    public void getPartialStringOneCharAndOneLonger() {
        assertEquals("rother", non.getPartialString("h", "Brother"));
    }

    @Test
    public void getPartialStringBothLonger() {
        assertEquals("HHBROTHER", non.getPartialString("HH", "BHBROTHER"));
    }
}