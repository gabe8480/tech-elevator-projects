package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CigarPartyTest {

    private CigarParty cig;

    @Before
    public void setUp() throws Exception {
        cig = new CigarParty();
    }

    @Test
    public void havePartyLowerEndAndNotWeekend() {
        assertTrue(cig.haveParty(40, false));
    }

    @Test
    public void havePartyUpperEndAndNotWeekend() {
        assertTrue(cig.haveParty(60, false));
    }

    @Test
    public void havePartyUpperEndAndWeekend() {
        assertTrue(cig.haveParty(730, true));
    }

    @Test
    public void havePartyNotEnoughAndNotWeekend() {
        assertFalse(cig.haveParty(39, false));
    }

    @Test
    public void havePartyTooMuchAndNotWeekend() {
        assertFalse(cig.haveParty(61, false));
    }

    @Test
    public void havePartyNotEnoughAndWeekend() {
        assertFalse(cig.haveParty(39, false));
    }
}