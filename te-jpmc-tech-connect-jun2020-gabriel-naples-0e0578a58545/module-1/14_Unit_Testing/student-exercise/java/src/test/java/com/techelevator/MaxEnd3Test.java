package com.techelevator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MaxEnd3Test {

    private MaxEnd3 max;

    @Before
    public void setUp() throws Exception {
        max = new MaxEnd3();
    }

    @Test
    public void makeArrayFrontBigger() {
        int[] nums = new int[]{11, 4, 7};
        int[] actual = new int[]{11,11,11};
        assertArrayEquals(actual, max.makeArray(nums));
    }

    @Test
    public void makeArrayBackBigger() {
        int[] nums = new int[]{1, 4, 7};
        int[] actual = new int[]{7,7,7};
        assertArrayEquals(actual, max.makeArray(nums));
    }

    @Test
    public void makeArraySame() {
        int[] nums = new int[]{11, 4, 11};
        int[] actual = new int[]{11,11,11};
        assertArrayEquals(actual, max.makeArray(nums));
    }

    @Test
    public void makeArraySameEndsMiddleBiggest() {
        int[] nums = new int[]{11, 40, 11};
        int[] actual = new int[]{11,11,11};
        assertArrayEquals(actual, max.makeArray(nums));
    }
}