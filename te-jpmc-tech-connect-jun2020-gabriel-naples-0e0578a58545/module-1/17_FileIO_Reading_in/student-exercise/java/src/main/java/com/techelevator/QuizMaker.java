package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class QuizMaker {

	public static void main(String[] args) {
		String answer = "";
		int correctTally = 0;
		int totalQuestions = 0;
		int answerChoice = 0;
		int answerNum = 0;
		Scanner userIn = new Scanner(System.in);

		try (Scanner quizFileReader = new Scanner(new File("test_quiz.txt"));){
			while(quizFileReader.hasNext()){
				String line = quizFileReader.nextLine();
				String[] questionAnswers = line.split("[/|]");
				System.out.println(questionAnswers[0]);
				totalQuestions++;
				for (int i = 1; i < questionAnswers.length; i++){
					if(questionAnswers[i].contains("*")){
						answer = questionAnswers[i].substring(0,questionAnswers[i].length()-1);
						answerNum = i;
						System.out.println(i+". " + answer);
					} else {
						System.out.println(i + ". " + questionAnswers[i]);
					}
				}
				System.out.println("\nYour Answer: ");
				answerChoice = Integer.parseInt(userIn.nextLine());
				if(answerChoice == answerNum){
					System.out.println("Correct");
					correctTally++;
				} else {
					System.out.println("Sorry that isn't correct!");
				}
			}
			System.out.println("\nYou got "+ correctTally +" answer(s) correct our of the total " + totalQuestions + " questions asked.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
