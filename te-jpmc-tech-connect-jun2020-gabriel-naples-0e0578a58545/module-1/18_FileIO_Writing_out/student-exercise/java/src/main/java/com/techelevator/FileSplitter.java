package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileSplitter {

	public static void main(String[] args) {

		Scanner userInput = new Scanner(System.in);

		System.out.println("Where is the input file (please include the path to the file)?");
		String filePath = userInput.nextLine();

		System.out.println("How many lines of text (max) should there be in the split files?");
		int linesPerFile = Integer.parseInt(userInput.nextLine());
		File inputFile = new File(filePath);
		int totalLineInFile = lineCounter(inputFile);

		System.out.println("The input file has "+totalLineInFile+" lines of text.\n");
		int numOfFiles = numOfFilesCalc(linesPerFile, totalLineInFile);
		System.out.println("\nFor a "+totalLineInFile+" line input file \""+filePath+"\", this produces "+numOfFiles+" output files.");
		generateFiles(inputFile, linesPerFile, numOfFiles);

	}

	public static int lineCounter(File file){
		int numOfLines = 0;
		try (Scanner fileScanner = new Scanner(file)){
			while(fileScanner.hasNext()){
				fileScanner.nextLine();
				numOfLines++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return numOfLines;
	}

	public static int numOfFilesCalc(int goalLinesPerFile, int totalLinesInFile){
		int filesNeeded = (int)Math.ceil((double)totalLinesInFile/(double)goalLinesPerFile);
		return filesNeeded;
	}

	public static void generateFiles(File file, int linesPerFile, int totalFiles){
		System.out.println("**GENERATING OUTPUT**\n");
		int i = 1;
		int j = 1;
		try (Scanner dataInput = new Scanner(file);){
			while(i <= totalFiles){
				String outputFile = "input-"+i+".txt";
				try(PrintWriter fileWriter = new PrintWriter(outputFile);){
					while (j <= linesPerFile && dataInput.hasNext()) {
						System.out.println("Generating " + outputFile);
						String output = dataInput.nextLine();
						fileWriter.println(output);
						j++;
					}
				} catch (FileNotFoundException ex){
					ex.getMessage();
				}

				j = 1;
				i++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}


	}



}
