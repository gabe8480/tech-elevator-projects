package com.techelevator;

public class HomeworkAssignment {

    private int earnedMarks;
    private int possibleMarks;
    private String submitterName;
    private String letterGrade;

    //Constructor
    public HomeworkAssignment(int possibleMarks){
        this.possibleMarks = possibleMarks;
    }

    //Setters and Getters
    public void setEarnedMarks(int earnedMarks){
        this.earnedMarks = earnedMarks;
        //sets the letter grade when the earned grade is entered
        this.letterGrade = deriveLetterGrade(earnedMarks, this.possibleMarks);
    }
    public void setSubmitterName(String submitterName){
        this.submitterName = submitterName;
    }
    public int getEarnedMarks() {
        return earnedMarks;
    }
    public int getPossibleMarks() {
        return possibleMarks;
    }
    public String getLetterGrade() {
        return letterGrade;
    }
    public String getSubmitterName() {
        return submitterName;
    }

    private String deriveLetterGrade(int earnedMarks, int possibleMarks){
        double percentGrade = (double)earnedMarks/(double)possibleMarks;
        if(percentGrade >= .90){
            return "A";
        } else if (percentGrade >= .8 && percentGrade <= .89){
            return "B";
        } else if(percentGrade >= .7 && percentGrade <= .79){
            return "C";
        } else if(percentGrade >= .6 && percentGrade <= .69){
            return "D";
        } else {
            return "F";
        }
    }

}
